# Dataset Paths
DATASET_JSON_FILE = "data/coco_annotations.json"
DATASET_FOLDER = "data"
TEST_IMAGES = "testingImages"

# Model Paths
# CONFIG_FILE_PATH = "COCO-Keypoints/keypoint_rcnn_R_101_FPN_3x.yaml"
CONFIG_FILE_PATH = "model/keypoint-windows-model/config.json"
MODEL_PATH = "model/keypoint-windows-model/model_final.pth"
# CONFIG_FILE_PATH = "COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml"
# MODEL_PATH = "model/model_final_101.pth"
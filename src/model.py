import os
import random
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

from src.paths import CONFIG_FILE_PATH, MODEL_PATH, TEST_IMAGES, DATASET_JSON_FILE, DATASET_FOLDER

from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2 import model_zoo
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.data.datasets import register_coco_instances
from detectron2.data.catalog import Metadata
from detectron2.config import CfgNode
import json

NUMBER_CLASSES = 1
SCORE_THRESHOLD = 0.7
# CLASSES = ["door"]
CLASSES = ["window"]


class Model():

    def __init__(self, classes = CLASSES, config_file_path = CONFIG_FILE_PATH, model_path = MODEL_PATH, score_threshold = SCORE_THRESHOLD):
        self.metadata = self.set_metadata(classes)
        self.cfg = self.setup_cfg(config_file_path, model_path, len(classes), score_threshold)
        self.predictor = DefaultPredictor(self.cfg)

    def set_metadata(self, classes):
        metadata = Metadata()
        metadata.set(thing_classes = classes)
        keypoint_names = ['top_left', 'top_right', 'bottom_left', 'bottom_right']
        keypoint_flip_map = [('top_left', 'top_right'), ('top_left', 'bottom_left'), ('top_right', 'bottom_right'),('bottom_left', 'bottom_right')]
        keypoint_connection_rules =  [
        ("top_left", "top_right", (0, 0, 255)),
        ('top_left', 'bottom_left', (0, 255, 0)),
        ('top_right', 'bottom_right', (255, 0, 0)),
        ('bottom_left', 'bottom_right', (255, 255, 0)),
        ]


        metadata.set(keypoint_names = keypoint_names)
        metadata.set(keypoint_flip_map = keypoint_flip_map)
        # metadata.set(keypoint_connection_rules = keypoint_connection_rules)


        return metadata

    def setup_cfg(self, config_file_path, model_path, number_classes, score_threshold):
        cfg = get_cfg()
        # cfg.merge_from_file(model_zoo.get_config_file(config_file_path))
        cfg.VAL_LOG_PERIOD = 1000
        cfg.WEIGHT_DECAY = 0.0001
        cfg.merge_from_file(config_file_path)
        cfg.MODEL.WEIGHTS = model_path
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = number_classes
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_threshold  # set the testing threshold for this model

        cfg.merge_from_list(["MODEL.DEVICE", "cpu", "MODEL.ROI_KEYPOINT_HEAD.NUM_KEYPOINTS", 4])
        # cfg.MODEL.ROI_KEYPOINT_HEAD.NUM_KEYPOINTS = 4
        cfg.TEST.KEYPOINT_OKS_SIGMAS = np.ones((4, 1), dtype=float).tolist()

        
        cfg.MODEL.DEVICE = 'cpu'
        cfg.freeze()
        # print(cfg)
        # with open(config_file_path) as f:
        #     # cfg = CfgNode(json.load(f))
        #     from pathlib import Path
        #     pth = Path(config_file_path)
        #     cfg = CfgNode.load_yaml_with_base(f)
        # cfg = CfgNode.load_yaml_with_base(config_file_path, allow_unsafe=True)

        # cfg.MODEL.WEIGHTS = model_path
        # cfg.MODEL.DEVICE = 'cpu'
        return cfg

    def predict(self, img):

        # img = cv2.resize(img, (512, 512))
        outputs = self.predictor(img)
        # print(outputs["instances"].pred_classes.tolist())
        # print(outputs['instances'].pred_boxes.tensor.numpy().tolist())
        # print('---------')
        # print(outputs["instances"].scores.tolist())
        # print(outputs['instances'].pred_keypoints.numpy().tolist())
        # print(self.metadata)
        outputs_dict = outputs["instances"].__dict__['_fields']
        fmt_out = {}
        fmt_out["image_height"]= img.shape[0]
        fmt_out["image_width"]= img.shape[1]
        fmt_out["pred_boxes"]= outputs["instances"].to("cpu").pred_boxes.tensor.tolist()
        fmt_out["scores"]= outputs["instances"].to("cpu").scores.tolist()
        fmt_out["pred_classes"]= outputs["instances"].to("cpu").pred_classes.tolist()
        if "pred_keypoints" in outputs_dict:
            fmt_out["keypoints"]= outputs['instances'].to("cpu").pred_keypoints.numpy().tolist()
        if "pred_masks" in outputs_dict:
            fmt_out["pred_masks"]= outputs['instances'].to("cpu").pred_masks.numpy().tolist()
        
        print(fmt_out)
        json.dump(fmt_out, open("output.json", "w"))
        v = Visualizer(img[:, :, ::-1],
                        metadata=self.metadata, 
                        scale=1)
        v = v.draw_instance_predictions(outputs['instances'].to('cpu'))
        # cv2.imshow('1', cv2.cvtColor(v.get_image()[:, :, ::-1], cv2.COLOR_BGR2RGB))
        # return v.get_image()[:, :, ::-1]
        return Image.fromarray(np.uint8(cv2.cvtColor(v.get_image()[:, :, ::-1], cv2.COLOR_BGR2RGB)))
        
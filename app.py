import cv2
import os
import requests
import io
from PIL import Image

from src.model import Model

from flask import Flask, render_template, request, send_from_directory, send_file
app = Flask(__name__)

model = Model()
# img = cv2.imread("testingImages/ALL-AMERICAN-WINDOW-AND-DOOR.jpg")
# pred = model.predict(img)

# cv2.imshow('1',pred)
# cv2.waitKey(0)

# function to load img from url
def load_image_url(url):
    response = requests.get(url)
    img = Image.open(io.BytesIO(response.content))
    return img

@app.route("/")
def index():

    # render the index.html template
    return render_template('index.html')


@app.route("/detect", methods=['POST', 'GET'])
def upload():
    if request.method == 'POST':

        try:
            # open image
            file = Image.open(request.files['file'].stream)

            # remove alpha channel
            rgb_im = file.convert('RGB')
            rgb_im.save('file.jpg')
        
        # failure
        except:

            return render_template("failure.html")

    elif request.method == 'GET':

        # get url
        url = request.args.get("url")

        # save
        try:
            # save image as jpg
            # urllib.request.urlretrieve(url, 'file.jpg')
            rgb_im = load_image_url(url)
            rgb_im = rgb_im.convert('RGB')
            rgb_im.save('file.jpg')

        # failure
        except:
            return render_template("failure.html")


    # run inference
    img = cv2.imread('file.jpg')

    result_img = model.predict(img)

    # create file-object in memory
    file_object = io.BytesIO()

    # write PNG in file-object
    result_img.save('file.png', 'PNG')

    # move to beginning of file so `send_file()` it will read from start    
    # file_object.seek(0)

    return send_file('file.png', mimetype='image/png')

# https://www.americanhomedesign.com/wp-content/uploads/product-double-hung.jpg

if __name__ == "__main__":

    # get port. Default to 8080
    # port = int(os.environ.get('PORT', 8080))

    # run app
    app.run(debug=True) # port=port)



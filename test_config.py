from detectron2.config import CfgNode
import json

# with open(str('model/aws_keypoints_R101_50000steps/metrics.json')) as fid:
#         cfg = CfgNode(json.load(fid))

# print(cfg)

# with open("test.json") as f:
#     data = json.load(f)

# print(data)


from pathlib import Path
path_cfg, path_model = None, None

path_models = []
for p_file in Path("model").iterdir():
    if p_file.name == "config.json":
        path_cfg = p_file
    if p_file.suffix == ".pth":
        path_models.append(p_file)

path_models = sorted(path_models)
path_model = path_models[int("1")]

print(path_models)

print(f"Using configuration specified in {path_cfg}")
print(f"Using model saved at {path_model}")
# opts = eval(json.dumps(["MODEL.DEVICE", "cpu", "MODEL.ROI_KEYPOINT_HEAD.NUM_KEYPOINTS", 4, "SOLVER.STEPS", (1500, 2000)]))
# for i in opts:
#     print(type(i))
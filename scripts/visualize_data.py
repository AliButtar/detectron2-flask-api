from paths import DATASET_FOLDER, DATASET_JSON_FILE

import cv2
import matplotlib.pyplot as plt
import random

from detectron2.data.datasets import register_coco_instances
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog

def register_dataset(name):

    register_coco_instances(name, {}, DATASET_JSON_FILE, DATASET_FOLDER)

    dataset_dicts = DatasetCatalog.get(name)
    metadata = MetadataCatalog.get(name)

    return dataset_dicts, metadata

dataset_dicts, metadata = register_dataset("windows_dataset")

for d in random.sample(dataset_dicts, 3):
    img = cv2.imread(d["file_name"])
    v = Visualizer(img[:, :, ::-1], metadata=metadata, scale=1)
    v = v.draw_dataset_dict(d)
    plt.figure(figsize = (14, 10))
    plt.imshow(cv2.cvtColor(v.get_image()[:, :, ::-1], cv2.COLOR_BGR2RGB))
    plt.show()
import os
import random
import cv2
import matplotlib.pyplot as plt

from src.paths import CONFIG_FILE_PATH, MODEL_PATH, TEST_IMAGES, DATASET_JSON_FILE, DATASET_FOLDER

from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer, ColorMode
from detectron2 import model_zoo
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.data.datasets import register_coco_instances


NUMBER_CLASSES = 1
SCORE_THRESHOLD = 0.5

# def register_dataset(name):

#     register_coco_instances(name, {}, DATASET_JSON_FILE, DATASET_FOLDER)

#     dataset_dicts = DatasetCatalog.get(name)
#     metadata = MetadataCatalog.get(name)

#     return dataset_dicts, metadata

# dataset_dicts, metadata = register_dataset("windows_dataset")


MetadataCatalog.get("windows_dataset").thing_classes = ['window']
metadata = MetadataCatalog.get('windows_dataset')
# print(metadata)

def setup_cfg(config_file_path, model_path, number_classes = 1, score_theshold = 0.5):
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file(config_file_path))
    cfg.MODEL.WEIGHTS = model_path
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = number_classes
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = score_theshold  # set the testing threshold for this model
    cfg.MODEL.DEVICE = 'cpu'
    cfg.freeze()
    return cfg


def model():
    cfg = setup_cfg(CONFIG_FILE_PATH, MODEL_PATH, NUMBER_CLASSES, SCORE_THRESHOLD)
    predictor = DefaultPredictor(cfg)
    return predictor

model = model()

test_images = []
for root, dirs, files in os.walk(TEST_IMAGES):
    for file in files:
        test_images.append(os.path.join(root, file))

## custom Image 
for i, path in enumerate(random.sample(test_images, 1)):
    
    imCm = cv2.imread(path)
    imCm = cv2.resize(imCm, (512, 512))
    outputsCm = model(imCm)
    v = Visualizer(imCm[:, :, ::-1],
                        metadata=metadata, 
                        scale=1, 
                        # instance_mode=ColorMode.IMAGE_BW   # remove the colors of unsegmented pixels
                    )
    v = v.draw_instance_predictions(outputsCm['instances'].to('cpu'))

    cv2.imshow(f'{i+1}', cv2.cvtColor(v.get_image()[:, :, ::-1], cv2.COLOR_BGR2RGB))
    
cv2.waitKey(0)